
-- SUMMARY --

The Commerce Piwik module adds e-commerce specific analytics with Piwik for
Drupal Commerce.
See: http://piwik.org/docs/ecommerce-analytics/

For a full description of the module, visit the project page:
  http://drupal.org/UPDATE_LINK

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/UPDATE_LINK


-- REQUIREMENTS --

* Drupal Commerce (http://drupal.org/project/commerce)
* Piwik Web Analytics (http://drupal.org/project/piwik)


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

ToDo...


-- CUSTOMIZATION --

n/a


-- TROUBLESHOOTING --

n/a


-- FAQ --

n/a


-- CONTACT --

