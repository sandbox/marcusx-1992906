<?php

/**
 * Create an array that can easily be converted to a piwik js action.
 *
 * Helper function to create an valid action array.
 * For better understanding we use associative arrays but later the keys
 * are stripped of and the piwik js array does only care about the order of
 * the values. Use this function so you don't need to care about the
 * array structure.
 *
 * @param string $function
 *   The piwik function name. Can be:
 *   - addEcommerceItem
 *   - trackEcommerceOrder
 *   - trackEcommerceCartUpdate
 *   - setEcommerceView
 *     If you use 'setEcommerceView' you need to set $args['view mode'] to
 *     'category' if it is a category page. Don't set it if it is a product
 *     page.
 *
 * @param array $args
 *   An associative array with data for sending to piwik. Valid keys:
 *   - For 'addEcommerceItem'
 *     - SKU
 *     - product name
 *     - product category (string or array with up to 5 categories)
 *     - product price
 *     - product quantity
 *   - For 'trackEcommerceOrder'
 *     - order ID (Unique order ID)
 *     - order total (Order total = tax, shipping, and subtracted discount)
 *     - order subtotal (Order sub total = excludes shipping)
 *     - order tax (Tax amount)
 *     - order shipping (Shipping amount)
 *     - order discount (Discount offered)
 *   - For 'trackEcommerceCartUpdate'
 *     - cart total (Cart amount)
 *   - For 'setEcommerceView'
 *     - SKU
 *     - product name
 *     - product category (string or array with up to 5 categories)
 *     - product price
 *
 * @return array
 *   Array you can convert to a json array for piwik.
 *
 */
function commerce_piwik_action_create($function, $args) {

  switch ($function){
    case 'addEcommerceItem':
      $action = array(
        'piwik operation' => 'addEcommerceItem',
        'SKU' => $args['SKU'],
        'product name' => $args['product name'],
        'product category' => $args['product category'],
        'product price' => $args['product price'],
        'product quantity' => $args['product quantity'],
      );
      break;
    case 'trackEcommerceOrder':
      $action = array(
        'piwik operation' => 'trackEcommerceOrder',
        'order ID' => $args['order ID'],
        'order total' => $args['order total'],
        'order subtotal' => $args['order subtotal'],
        'order tax' => $args['order tax'],
        'order shipping' => $args['order shipping'],
        'order discount' => $args['order discount'],
      );
      break;
    case 'trackEcommerceCartUpdate':
      $action = array(
        'piwik operation' => 'trackEcommerceCartUpdate',
        'cart total' => $args['cart total'],
      );
      break;
    case 'setEcommerceView':
      // setEcommerceView is used for category and product pages.
      // On category pages SKU and product name needs to be false.
      if (isset($args['view mode']) && $args['view mode'] == 'category') {
        $action = array(
          'piwik operation' => 'setEcommerceView',
          // No product on category page.
          'SKU' => FALSE,
          // No product on category page.
          'product name' => FALSE,
          // Product category, or array of up to 5 categories.
          'product category' => $args['product category'],
        );
      }
      else {
        $action = array(
          'piwik operation' => 'setEcommerceView',
          'SKU' => $args['SKU'],
          'product name' => $args['product name'],
          'product category' => $args['product category'],
          'product price' => $args['product price'],
        );
      }
      break;
    default:
      $action = array();
      break;
  }

  return $action;
}

/**
 * Turns an array of actions arrays into a piwik array push script.
 *
 * @param array $actions
 *   Array of actions we wan't to send to piwik.
 *
 * @return string
 *   A string of javascript for injection into the page.
 *
 */
function commerce_piwik_actions2js($actions) {
  $script = '';

  foreach ($actions as $action){
    $action_json = drupal_json_encode(array_values($action));
    $script .= "_paq.push(" . $action_json . ");\n";
  }

  return $script;
}
