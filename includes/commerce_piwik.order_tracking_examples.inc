<?php

function commerce_piwik_get_order_details() {

  $actions = array();

  $actions[] = commerce_piwik_action_create('addEcommerceItem', array(
    'SKU' => '0815',
    'product name' => 'Product 1',
    'product category' => 'Category 1',
    'product price' => 12.13,
    'product quantity' => 1,
  ));

  $actions[] = commerce_piwik_action_create('addEcommerceItem', array(
    'SKU' => '4711',
    'product name' => 'Product 2',
    'product category' => 'Category 2',
    'product price' => 17.22,
    'product quantity' => 22,
  ));

  $actions[] = commerce_piwik_action_create('trackEcommerceOrder', array(
    'order ID' => '124',  // Unique order ID.
    'order total' => 35, // Order total (tax, shipping, and subtracted discount).
    'order subtotal' => 30, // Order sub total (excludes shipping).
    'order tax' => 5.5, // Tax amount.
    'order shipping' => 4.5, // Shipping amount.
    'order discount' => FALSE, // Discount offered.
  ));

  return $actions;
}

/**
 * Build the e-commerce JS passed to piwik for cart tracking.
 *
 * @param object $order
 *   The fully loaded order object to convert into GA JS.
 *
 * @return string
 *   The JS that should be added to the page footer.
 */

function commerce_piwik_get_cart_details() {

  $actions = array();

  $actions[] = commerce_piwik_action_create('addEcommerceItem', array(
    'SKU' => '1267',
    'product name' => 'Product 17', //Product name.
    'product category' => array('Category 10', 'Category 12' , 'Category 15'),
    'product price' => 8.7,
    'product quantity' => 4,
  ));

  $actions[] = commerce_piwik_action_create('addEcommerceItem', array(
    'SKU' => '9934',
    'product name' => 'Product 25',
    'product category' => 'Category 7',
    'product price' => 17.22,
    'product quantity' => 11,
  ));

  $actions[] = commerce_piwik_action_create('trackEcommerceCartUpdate', array(
    'cart total' => 56.40, // Cart amount.
  ));

  return $actions;
}

/**
 * Build the e-commerce JS passed to piwik for order tracking.
 *
 * @param object $order
 *   The fully loaded order object to convert into GA JS.
 *
 * @return string
 *   The JS that should be added to the page footer.
 */
function commerce_piwik_get_category_view_details() {

  $actions = array();

  $actions[] = commerce_piwik_action_create('setEcommerceView', array(
    'view mode' => 'category',
    'product category' => array('Bubu', 'Hurz', 'Gulugulu'),
  ));

  $actions[] = commerce_piwik_action_create('setEcommerceView', array(
    'view mode' => 'category',
    'product category' => array('Bubu', 'Hurz', 'Gulugulu'),
  ));

  $actions[] = commerce_piwik_action_create('setEcommerceView', array(
    'view mode' => 'category',
    'product category' => array('Bubu', 'Hurz', 'Gulugulu'),
  ));

  return $actions;
}


function commerce_piwik_get_product_view_details() {

  $actions = array();

  $actions[] = commerce_piwik_action_create('setEcommerceView', array(
    'SKU' => '0815',
    'product name' => 'Product 1',
    'product category' => 'Category 23',
    'product price' => 12.13,
  ));

  $actions[] = commerce_piwik_action_create('setEcommerceView', array(
    'SKU' => '4711',
    'product name' => 'Product 2',
    'product category' => 'Category 17',
    'product price' => 17.22,
  ));

  $actions[] = commerce_piwik_action_create('setEcommerceView', array(
    'SKU' => '3478',
    'product name' => 'Product 45',
    'product category' => 'Category 77',
    'product price' => 17.22,
  ));

  return $actions;
}
